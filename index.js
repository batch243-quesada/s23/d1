// console.log('Hello, World');

// Objects
// a data type that is used to represent real world objects
// a collection of related data and/or functionalities/methods
// information stored in objects are represented in a key:value pair
// key is also referred to as "property" of an object
// different data types may be stored in an object's property creating data structures

// Crearting objects using object initializers / literal notation
// let/const objectName = {
// 						keyA: valueA,
// 						keyB: valueB
// 					};
// This creates/declares an object and also initializes/assign it's properties upon creation
// a phone is an example of a real world object
// it has its own properties such as name, color, weight, unit model, and a lot of other properties
let phone = {
	name: 'Nokia 3210',
	manufactureDate: 1999
};

console.log(phone);

///// Creating objects using constructor function /////
// creates a reusable function to create several objects that have the same data structure
// This is useful for creating multiple instances/copies of an objeect
// An instance is a concreate occurence of any object which emphasizes distinct/unique identity of it
// function objectName(valueA, valueB) {
// 	this.keyA = valueA,
// 	this.keyB = valueB
// }

// this - keyword that allows us to assign a new object's properties by associating them with values received from a constructor function's parameter
function Laptop(name, manufactureDate, ram) {
	this.laptopName = name,
	this.laptopManufactureDate = manufactureDate,
	this.laptopRam = ram
}

// Instantiation
// the 'new' operator creates an instances of an object
// objects and instances are often interchanged because object literals(let object ={}) and instances (let obejctName = new functionName(arguments)) are distinct/unique objects
let laptop = new Laptop('Lenovo', 2008, '4gb');
console.log(laptop);

let myLaptop = new Laptop('MacBook Air', 2020, '8gb');
console.log(myLaptop);

console.log(laptop.laptopName);
console.log(laptop.laptopManufactureDate);

///// Mini acitivity ///
function Menu(name, price) {
	this.menuName  = name,
	this.menuPrice = price
}

let menu1 = new Menu('Yumburger', 35);
console.log(menu1);

// creating empty objects
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

// accessing objects inside an array
let array = [laptop, myLaptop];
console.log(array);
console.log(array[0].laptopName);
console.log(array[0]['laptopName']);

////// Initializing/adding/deleting/reassigning Object properties ////
// like any other variable in JS, objects have their properties initialized/added after the object was created/declared
let car = {};
console.log(car);
// Initializing/adding object properties using dot notation
car.name = 'Honda Civic';
car.speed = '40kph';
console.log(car);

// Initializing/adding object property using bracket notation
car['manufactureDate'] = '2019';
console.log(car);

// deleting object properties
delete car['speed'];
console.log(car);

// delete car.manufactureDate;
// console.log(car);

// reassign object properties
car.name = 'Mitsubishi Lancer';
console.log(car);

car['name'] = 'Ford Ranger';
console.log(car);


////// Object Methods //////
// a method is a function which is a property of an object
// they are also functions and one of the key differences they have is that methods are functions related to a specific objeect
let person = {
	name: 'John',
	talk: function() {
		console.log(`Hello, my name is ${this.name}`);
	},
	action() {
		console.log(`I love running.`);
	},
	age: 25
}

console.log(person);
person.talk();

// add method to objects
person.walk = function() {
	console.log(`${this.name} walked 25 steps forward`);
}

console.log(person);
person.walk();

// methods are useful for creating reusable functions that perform tasks related to objects
let friends = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		state: 'Texas'
	},
	emails: [
		'joe@mail.com',
		'joesmith@email.xyz'
		],
	introduce() {
		console.log(`Hello, my name is ${this.firstName} ${this.lastName}. I live in ${this['address']['state']} and ${this.address.city} City. My emails are ${this.emails[0]} and ${this.emails[1]}.`);
	}
}

console.log(friends);
friends.introduce();

///// example /////
function Pokemon(name, level) {
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth = 2 * level;
	this.pokemonAttack = level;

	// let's add a method named tackle
	this.tackle = function(target) {
		console.log(`${this.pokemonName} tackles ${target.pokemonName}`)
	}

	this.faint = function() {
		console.log(`${this.pokemonName} fainted!`)
	}
}

let pikachu = new Pokemon('Pikachu', 12);
console.log(pikachu);
let groudon = new Pokemon('Groudon', 100);
console.log(groudon);
pikachu.tackle(groudon);
groudon.faint();

